# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\zhangfan\dev\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#Other
-dontwarn kotlin.**
-keep class com.ultrapower.** {*;}
-keep class uk.co.senab.** {*;}

#Project
-keep class app.muyiafan.ngabbs.bean.** {*;}
-keep class app.muyiafan.ngabbs.MainActivity {*;}
-keep class app.muyiafan.ngabbs.MainApplication {*;}
-keep class app.muyiafan.kotlin.** {*;}
-keeppackagenames org.jsoup.nodes