# remove Log
-assumenosideeffects class android.util.Log{
    public static int v(...);
    public static int i(...);
    public static int d(...);
    public static int w(...);
    public static int e(...);
}

-assumenosideeffects class java.io.PrintStream{
    public void println(%);
    public void println(**);
}