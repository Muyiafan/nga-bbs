#RongCloud
-keepclassmembers class fqcn.of.javascript.interface.for.webview {
 public *;
}
-keepattributes Exceptions,InnerClasses
-keepattributes Signature
-keepattributes *Annotation*
-keep class com.google.gson.examples.android.model.** { *; }
-keep class **$Properties
-dontwarn org.eclipse.jdt.annotation.**
-keep class io.rong.** {*;}
-keep class io.agora.rtc.** {*; }
-keep class * implements io.rong.imlib.model.MessageContent{*;}
-dontwarn io.rong.push.**
-dontnote com.xiaomi.**
-dontnote com.huawei.android.pushagent.**
-dontnote com.google.android.gms.gcm.**
-dontnote io.rong.**