/*
 * Copyright (c) 2016. Muyiafan
 */

package app.muyiafan.ngabbs.utils

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/7.
 */
object Api {
    val NGA_URL = "http://nga.178.com"
    val OWN_SERVER = "http://api.muyiafan.tk:9581"
}