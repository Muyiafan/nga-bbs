package app.muyiafan.ngabbs.bean;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import app.muyiafan.kotlin.adapter.HolderContent;

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/1.
 */

public class NgaMain implements HolderContent {
    @SerializedName("all")
    @Expose
    private List<All> all = new ArrayList<All>();
    @SerializedName("imgBase")
    @Expose
    private String imgBase;

    /**
     * @return The all
     */
    public List<All> getAll() {
        return all;
    }

    /**
     * @param all The all
     */
    public void setAll(List<All> all) {
        this.all = all;
    }

    /**
     * @return The imgBase
     */
    public String getImgBase() {
        return imgBase;
    }

    /**
     * @param imgBase The imgBase
     */
    public void setImgBase(String imgBase) {
        this.imgBase = imgBase;
    }

    public class Content implements HolderContent {
        @SerializedName("fid")
        @Expose
        private Integer fid;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("infoL")
        @Expose
        private String infoL;
        @SerializedName("bit")
        @Expose
        private Integer bit;

        /**
         * @return The fid
         */
        public Integer getFid() {
            return fid;
        }

        /**
         * @param fid The fid
         */
        public void setFid(Integer fid) {
            this.fid = fid;
        }

        /**
         * @return The name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The infoL
         */
        public String getInfoL() {
            return infoL;
        }

        /**
         * @param infoL The infoL
         */
        public void setInfoL(String infoL) {
            this.infoL = infoL;
        }

        /**
         * @return The bit
         */
        public Integer getBit() {
            return bit;
        }

        /**
         * @param bit The bit
         */
        public void setBit(Integer bit) {
            this.bit = bit;
        }

    }

    public class All implements HolderContent {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("content")
        @Expose
        private List<Content> content = new ArrayList<Content>();

        /**
         * @return The id
         */
        public String getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         * @return The name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The content
         */
        public List<Content> getContent() {
            return content;
        }

        /**
         * @param content The content
         */
        public void setContent(List<Content> content) {
            this.content = content;
        }

    }
}
