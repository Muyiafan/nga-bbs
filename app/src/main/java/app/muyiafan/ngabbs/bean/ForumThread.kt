package app.muyiafan.ngabbs.bean

import app.muyiafan.kotlin.adapter.HolderContent

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/6.
 */

data class ForumThread(
        val title: String,
        val url: String,
        val master: String,
        val date: String,
        val count: Int,
        val lastComment: String
) : HolderContent