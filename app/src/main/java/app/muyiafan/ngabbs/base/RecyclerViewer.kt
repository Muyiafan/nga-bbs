package app.muyiafan.ngabbs.base

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/2.
 */

interface RecyclerViewer {
    fun firstLoad()
    fun loadCompleted()
    fun loadError()
    fun fetchDown(start: Int, count: Int)
    fun fetchUp()
}