package app.muyiafan.ngabbs.base

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.muyiafan.kotlin.adapter.HolderContent
import app.muyiafan.ngabbs.R
import kotlinx.android.synthetic.main.fragment_recycler_view.view.*

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/1.
 */
abstract class RecyclerViewFragment : Fragment(), RecyclerViewer {
    protected val contentRCV by lazy { view.contentRCV }
    protected val swipeRL by lazy { view.swipeRL }
    protected val itemList: MutableList<HolderContent> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_recycler_view, container, false)
    }
}