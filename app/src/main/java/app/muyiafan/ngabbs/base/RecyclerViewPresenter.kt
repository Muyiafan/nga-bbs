package app.muyiafan.ngabbs.base

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/2.
 */

interface RecyclerViewPresenter {
    fun onStart()
    fun fetchDown()
}