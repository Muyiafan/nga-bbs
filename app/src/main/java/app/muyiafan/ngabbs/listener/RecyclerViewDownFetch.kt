/*
 * Copyright (c) 2016. Muyiafan
 */

package app.muyiafan.ngabbs.listener

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import app.muyiafan.ngabbs.base.RecyclerViewPresenter

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/9.
 */
class RecyclerViewDownFetch(val presenter: RecyclerViewPresenter) : RecyclerView.OnScrollListener() {
    var isRefreshing = false
    private var firstVisible = 0
    private var lastVisible = 0
    private var isLastItem = false

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        val layoutManager = recyclerView.layoutManager
        when (layoutManager) {
            is LinearLayoutManager -> {
                firstVisible = layoutManager.findFirstCompletelyVisibleItemPosition()
                lastVisible = layoutManager.findLastCompletelyVisibleItemPosition()
            }
            is GridLayoutManager -> {
                firstVisible = layoutManager.findFirstCompletelyVisibleItemPosition()
                lastVisible = layoutManager.findLastCompletelyVisibleItemPosition()
            }
        }
        isLastItem = lastVisible + 1 == recyclerView.adapter.itemCount
        if (newState == RecyclerView.SCROLL_STATE_IDLE && isLastItem && !isRefreshing) {
            isRefreshing = true
            presenter.fetchDown()
        } else {
            Log.d("refresh", "is reflushing")
        }
    }
}