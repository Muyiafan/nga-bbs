package app.muyiafan.ngabbs

import android.app.Application
import android.content.Context
import android.graphics.Typeface
import app.muyiafan.kotlin.adapter.ViewHolderPool
import app.muyiafan.ngabbs.bean.ForumThread
import app.muyiafan.ngabbs.bean.NgaMain
import app.muyiafan.ngabbs.nexus.ForumGroupNexus
import app.muyiafan.ngabbs.nexus.ForumItemNexus
import app.muyiafan.ngabbs.nexus.ForumThreadNexus
import app.muyiafan.ngabbs.nexus.LoadingBarNexus

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/1.
 */
class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        awsomeFont = Typeface.createFromAsset(this.assets, "font/fontawesome-webfont.ttf")
        context = applicationContext
        register()
    }

    private fun register() {
        ViewHolderPool.register(NgaMain.Content::class.java, ForumItemNexus)
        ViewHolderPool.register(NgaMain.All::class.java, ForumGroupNexus)
        ViewHolderPool.register(ForumThread::class.java, ForumThreadNexus)
        ViewHolderPool.register(LoadingBarNexus.LoadingStatus::class.java, LoadingBarNexus)
    }

    companion object {
        lateinit var awsomeFont: Typeface
        lateinit var context: Context
    }
}