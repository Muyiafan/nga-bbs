/*
 * Copyright (c) 2016. Muyiafan
 */

package app.muyiafan.ngabbs.modules.thread

import android.os.Bundle
import android.webkit.WebViewFragment

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/7.
 */

class ThreadFragment : WebViewFragment() {
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val settings = webView.settings
        settings.javaScriptEnabled = true
        webView.loadUrl("http://nga.178.com/read.php?tid=9863672&_ff=459")
    }
}