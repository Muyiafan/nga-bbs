/*
 * Copyright (c) 2016. Muyiafan
 */

package app.muyiafan.ngabbs.modules.forum

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import app.muyiafan.kotlin.adapter.MultiTypeAdapter
import app.muyiafan.kotlin.showSnackBar
import app.muyiafan.ngabbs.base.RecyclerViewFragment
import app.muyiafan.ngabbs.listener.RecyclerViewDownFetch
import com.github.vmironov.jetpack.arguments.bindArgument

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/2.
 */
class ForumFragment : RecyclerViewFragment() {
    private val fid by bindArgument<Int>("fid")
    private val title by bindArgument<String>("forumName")
    private val presenter by lazy { ForumPresenter(this, itemList, fid) }
    private val downListener by lazy { RecyclerViewDownFetch(presenter) }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.title = title
        contentRCV.layoutManager = LinearLayoutManager(activity)
        contentRCV.adapter = MultiTypeAdapter(itemList)
        swipeRL.post {
            swipeRL.isRefreshing = true
            presenter.onStart()
        }
        swipeRL.setOnRefreshListener {
            swipeRL.isRefreshing = true
            presenter.onStart()
        }
        contentRCV.addOnScrollListener(downListener)
    }

    override fun fetchUp() {

    }

    override fun fetchDown(start: Int, count: Int) {
        contentRCV.adapter.notifyItemRangeInserted(start, count)
        loadCompleted()
    }

    override fun loadError() {
        showSnackBar("Network error")
        loadCompleted()
    }

    override fun loadCompleted() {
        swipeRL.isRefreshing = false
        downListener.isRefreshing = false
    }

    override fun firstLoad() {
        contentRCV.adapter.notifyDataSetChanged()
        loadCompleted()
    }

    override fun onDestroy() {
        activity.title = "NGA"
        contentRCV.addOnScrollListener(null)
        super.onDestroy()
    }

    companion object {
        fun newInstance(fid: Int, forumName: String) = ForumFragment().apply {
            arguments = Bundle().apply {
                putInt("fid", fid)
                putString("forumName", forumName)
            }
        }
    }
}