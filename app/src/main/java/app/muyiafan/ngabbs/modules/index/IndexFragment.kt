package app.muyiafan.ngabbs.modules.index

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import app.muyiafan.kotlin.adapter.MultiTypeAdapter
import app.muyiafan.kotlin.showSnackBar
import app.muyiafan.ngabbs.R
import app.muyiafan.ngabbs.base.RecyclerViewFragment

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/2.
 */
class IndexFragment : RecyclerViewFragment() {
    private val presenter = IndexPresenter(this, itemList)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        contentRCV.layoutManager = LinearLayoutManager(activity)
        contentRCV.adapter = MultiTypeAdapter(itemList)
        swipeRL.post {
            swipeRL.isRefreshing = true
            presenter.onStart()
        }
    }

    override fun firstLoad() {
        contentRCV.adapter.notifyDataSetChanged()
        loadCompleted()
    }

    override fun loadError() {
        showSnackBar("Net error")
        loadCompleted()
    }

    override fun loadCompleted() {
        swipeRL.isRefreshing = false
        swipeRL.isEnabled = false
    }

    override fun fetchUp() {
    }

    override fun fetchDown(start: Int, count: Int) {
    }

    companion object {
        fun newInstance() = IndexFragment().apply {
            arguments = Bundle().apply {
            }
        }
    }
}