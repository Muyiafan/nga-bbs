/*
 * Copyright (c) 2016. Muyiafan
 */

package app.muyiafan.ngabbs.modules.index

import app.muyiafan.kotlin.adapter.HolderContent
import app.muyiafan.kotlin.ioOrNet
import app.muyiafan.ngabbs.base.RecyclerViewPresenter
import app.muyiafan.ngabbs.base.RecyclerViewer
import app.muyiafan.ngabbs.bean.NgaMain
import app.muyiafan.ngabbs.utils.Api
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import rx.Observable

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/2.
 */
class IndexPresenter(val viewer: RecyclerViewer, val itemList: MutableList<HolderContent>) : RecyclerViewPresenter {
    override fun fetchDown() {
    }

    override fun onStart() {
        Observable.create<NgaMain> {
            val okhttp = OkHttpClient()
            val request = Request.Builder().url(Api.OWN_SERVER + "/nga").build()
            val response = okhttp.newCall(request).execute()
            val ngaMain = Gson().fromJson(response.body().string(), NgaMain::class.java)
            it.onNext(ngaMain)
        }.ioOrNet().subscribe {
            itemList.clear()
            it.all.forEach {
                itemList.add(it)
                it.content.forEach { itemList.add(it) }
            }
            viewer.firstLoad()
        }
    }
}