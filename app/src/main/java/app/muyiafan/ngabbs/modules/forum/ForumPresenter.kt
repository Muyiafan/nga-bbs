package app.muyiafan.ngabbs.modules.forum

import app.muyiafan.kotlin.adapter.HolderContent
import app.muyiafan.kotlin.ioOrNet
import app.muyiafan.ngabbs.base.RecyclerViewPresenter
import app.muyiafan.ngabbs.base.RecyclerViewer
import app.muyiafan.ngabbs.bean.ForumThread
import app.muyiafan.ngabbs.nexus.LoadingBarNexus
import app.muyiafan.ngabbs.utils.Api
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import rx.Observable
import java.text.SimpleDateFormat
import java.util.*

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/6.
 */
class ForumPresenter(val viewer: RecyclerViewer, val itemList: MutableList<HolderContent>, val fid: Int) : RecyclerViewPresenter {
    private var page = 1
    override fun onStart() {
        page = 1
        itemList.clear()
        Observable.create<List<ForumThread>> {
            val html = Jsoup.connect(getUrl()).get()
            val threads = html.select("#topicrows").select("tbody")
            it.onNext(threads.map { handlerHtmlToBean(it) })
        }.ioOrNet().subscribe(
                { date ->
                    itemList.addAll(date)
                    itemList.add(LoadingBarNexus.LoadingStatus.Loading)
                    viewer.firstLoad()
                    page++
                },
                { error ->
                    error.printStackTrace()
                    viewer.loadError()
                })
    }

    override fun fetchDown() {
        Observable.create<List<ForumThread>> {
            val html = Jsoup.connect(getUrl()).get()
            val threads = html.select("#topicrows").select("tbody")
            it.onNext(threads.map { handlerHtmlToBean(it) })
        }.ioOrNet().subscribe(
                { data ->
                    itemList.removeAt(itemList.size - 1)
                    itemList.addAll(data)
                    itemList.add(LoadingBarNexus.LoadingStatus.Loading)
                    viewer.fetchDown(itemList.size - data.size - 1, data.size)
                    page++
                },
                { error ->
                    error.printStackTrace()
                    viewer.loadError()
                })
    }

    private fun getUrl() = "${Api.NGA_URL}/thread.php?fid=$fid&page=$page"

    private fun handlerHtmlToBean(element: Element): ForumThread {
        val c1 = element.select(".c1").first()
        val c2 = element.select(".c2").first()
        val c3 = element.select(".c3").first()
        val c4 = element.select(".c4").first()

        val count = c1.select("a").text().toInt()

        val ta = c2.select("a")
        val title = ta.text()
        val url = ta.attr("href")

        val masterName = c3.select("a").text()
        val time = c3.select("span").text().toLong()
        val date = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.CHINA).format(time * 1000)

        val lastComment = c4.select("span").text()
        return ForumThread(title, url, masterName, date, count, lastComment).apply { println(this.toString()) }
    }
}
