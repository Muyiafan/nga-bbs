package app.muyiafan.ngabbs.nexus

import android.app.Activity
import android.graphics.Color
import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.muyiafan.kotlin.adapter.HolderBeanNexus
import app.muyiafan.kotlin.findViewByIdT
import app.muyiafan.ngabbs.MainApplication
import app.muyiafan.ngabbs.R
import app.muyiafan.ngabbs.bean.ForumThread
import app.muyiafan.ngabbs.utils.Api

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/6.
 */

object ForumThreadNexus : HolderBeanNexus<ForumThreadNexus.ThreadViewHolder, ForumThread> {
    override val tag: String = "ForumThreadNexus"
    private val resources = MainApplication.context.resources
    private val userIcon = resources.getString(R.string.fa_user)
    private val commentIcon = resources.getString(R.string.fa_comment)
    private val threadTag = arrayOf("攻略", "蓝贴", "活动", "吐槽", "公告", "赛事", "讨论")
    private val threadColors = arrayOf(
            resources.getColor(R.color.material_color_green_800, null),
            resources.getColor(R.color.material_color_light_blue_800, null),
            resources.getColor(R.color.material_color_pink_800, null),
            resources.getColor(R.color.material_color_yellow_800, null),
            resources.getColor(R.color.material_color_deep_purple_800, null),
            resources.getColor(R.color.material_color_red_800, null),
            resources.getColor(R.color.material_color_teal_800, null)
    )

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup) = ThreadViewHolder(
            inflater.inflate(R.layout.item_thread_view, parent, false)
    )

    override fun onBindViewHolder(holder: ThreadViewHolder, holderContent: ForumThread) {
        holder.titleTV.setTextColor(Color.BLACK)
        holder.titleTV.text = holderContent.title
        threadTag.forEachIndexed { i, tag ->
            if (holderContent.title.contains(tag)) holder.titleTV.setTextColor(threadColors[i])
        }
        holder.masterTV.typeface = MainApplication.awsomeFont
        holder.masterTV.text = userIcon + "  " + holderContent.master + "    " + holderContent.date

        holder.commentCountTV.typeface = MainApplication.awsomeFont
        holder.commentCountTV.text = "Last by ${holderContent.lastComment}    $commentIcon  ${holderContent.count}"
        val context = holder.itemView.context
        holder.itemView.setOnClickListener {
            //ThreadFragment().startFragmentA(R.id.mainFrameLayout, fm, "Thread")
            val builder = CustomTabsIntent.Builder()
            builder.setToolbarColor(context.resources.getColor(R.color.colorPrimary, null))
            val intent = builder.build()
            intent.launchUrl(context as Activity, Uri.parse(Api.NGA_URL + holderContent.url))
        }
    }

    class ThreadViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleTV = itemView.findViewByIdT<AppCompatTextView>(R.id.threadTitleTV)
        val masterTV = itemView.findViewByIdT<AppCompatTextView>(R.id.masterTV)
        val commentCountTV = itemView.findViewByIdT<AppCompatTextView>(R.id.commentCountTV)
    }
}