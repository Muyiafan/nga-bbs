package app.muyiafan.ngabbs.nexus

import android.app.Activity
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.muyiafan.kotlin.adapter.HolderBeanNexus
import app.muyiafan.kotlin.findViewByIdT
import app.muyiafan.kotlin.startFragmentA
import app.muyiafan.ngabbs.R
import app.muyiafan.ngabbs.bean.NgaMain
import app.muyiafan.ngabbs.modules.forum.ForumFragment
import com.squareup.picasso.Picasso

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/2.
 */
object ForumItemNexus : HolderBeanNexus<ForumItemNexus.IndexForumHolder, NgaMain.Content> {
    override val tag: String = "ForumItemNexus"
    private val baseUrl = "http://img4.nga.178.com/ngabbs/nga_classic/f/$tag.png"

    override fun onBindViewHolder(holder: IndexForumHolder, holderContent: NgaMain.Content) {
        holder.forumNameTV.text = holderContent.name
        Picasso.with(holder.itemView.context)
                .load(baseUrl.replace(tag, holderContent.fid.toString()))
                .fit().centerCrop()
                .into(holder.forumIconIV)
        val fm = (holder.itemView.context as Activity).fragmentManager
        holder.itemView.setOnClickListener {
            ForumFragment.newInstance(holderContent.fid, holderContent.name).startFragmentA(R.id.mainFrameLayout, fm, "ForumFragment")
        }
    }

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup) = IndexForumHolder(
            inflater.inflate(R.layout.item_forum_view, parent, false)
    )

    class IndexForumHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val forumNameTV = itemView.findViewByIdT<AppCompatTextView>(R.id.forumNameTV)
        val forumIconIV = itemView.findViewByIdT<AppCompatImageView>(R.id.forumIconIV)
    }
}