/*
 * Copyright (c) 2016. Muyiafan
 */

package app.muyiafan.ngabbs.nexus

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import app.muyiafan.kotlin.adapter.HolderBeanNexus
import app.muyiafan.kotlin.adapter.HolderContent
import app.muyiafan.kotlin.findViewByIdT
import app.muyiafan.ngabbs.R

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by ZhangWeifan on 2016/9/8.
 */
object LoadingBarNexus : HolderBeanNexus<LoadingBarNexus.LoadingHolder, LoadingBarNexus.LoadingStatus> {
    override val tag: String = "LoadingBarNexus"

    override fun onBindViewHolder(holder: LoadingHolder, holderContent: LoadingStatus) {
        
    }

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup) = LoadingHolder(
            inflater.inflate(R.layout.item_loading_view, parent, false)
    )

    enum class LoadingStatus : HolderContent {
        Loading, Complete
    }

    class LoadingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val loadingPB = itemView.findViewByIdT<ProgressBar>(R.id.loadingPB)
    }
}