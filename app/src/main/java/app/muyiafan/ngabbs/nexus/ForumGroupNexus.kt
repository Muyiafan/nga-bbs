package app.muyiafan.ngabbs.nexus

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.muyiafan.kotlin.adapter.HolderBeanNexus
import app.muyiafan.kotlin.findViewByIdT
import app.muyiafan.ngabbs.bean.NgaMain

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/2.
 */
object ForumGroupNexus : HolderBeanNexus<ForumGroupNexus.ForumGroupHolder, NgaMain.All> {
    override val tag: String = "ForumGroup"

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup) = ForumGroupHolder(
            inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
    )

    override fun onBindViewHolder(holder: ForumGroupHolder, holderContent: NgaMain.All) {
        holder.groupTitleTV.text = holderContent.name
    }

    class ForumGroupHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val groupTitleTV = itemView.findViewByIdT<TextView>(android.R.id.text1)
    }
}