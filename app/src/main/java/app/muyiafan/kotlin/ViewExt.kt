package app.muyiafan.kotlin

import android.content.Intent
import android.view.View

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by ZhangWeifan on 2016/7/29.
 */
inline fun <reified T : Any> View.intentFor(vararg params: Pair<String, Any>): Intent = Fun.createIntent(context, T::class.java, params)

inline fun <reified T : Any> View.startActivity(vararg params: Pair<String, Any>) {
    startActivity(Fun.createIntent(context, T::class.java, params))
}

fun View.startActivity(intent: Intent) {
    context.startActivity(intent)
}

@Suppress("UNCHECKED_CAST")
fun <T> View.findViewByIdT(id: Int) = this.findViewById(id) as T