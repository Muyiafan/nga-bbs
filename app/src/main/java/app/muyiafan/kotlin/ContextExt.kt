package app.muyiafan.kotlin

import android.content.Context
import android.content.Intent

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/7/29.
 */
inline fun <reified T : Any> Context.intentFor(vararg params: Pair<String, Any>): Intent = Fun.createIntent(this, T::class.java, params)
