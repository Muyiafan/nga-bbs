package app.muyiafan.kotlin

import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/9/6.
 */

fun <T> rx.Observable<T>.ioOrNet() = subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())