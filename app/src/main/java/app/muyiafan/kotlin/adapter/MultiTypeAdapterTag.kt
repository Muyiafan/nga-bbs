package app.muyiafan.kotlin.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by ZhangWeifan on 2016/8/17.
 */
class MultiTypeAdapterTag(val data: MutableList<Pair<String, HolderContent>>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val type = getItemViewType(position)
        val content = data[position].second
        ViewHolderPool.getNexusWithTag(type).onBind(holder, content)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = ViewHolderPool.getNexusWithTag(viewType).onCreateViewHolder(
            LayoutInflater.from(parent.context), parent
    )

    override fun getItemViewType(position: Int): Int = ViewHolderPool.getTagIndex(data[position].first)

    override fun getItemCount(): Int = data.size
}