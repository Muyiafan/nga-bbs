package app.muyiafan.kotlin.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by ZhangWeifan on 2016/8/12.
 */
interface HolderBeanNexus<V : RecyclerView.ViewHolder, in H : HolderContent> {
    val tag: String
    fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): V
    fun onBindViewHolder(holder: V, holderContent: H)

    @Suppress("UNCHECKED_CAST")
    fun onBind(holder: RecyclerView.ViewHolder, holderContent: HolderContent) {
        onBindViewHolder(holder as V, holderContent as H)
    }
}