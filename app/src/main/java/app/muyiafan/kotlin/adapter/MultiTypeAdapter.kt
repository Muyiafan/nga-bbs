package app.muyiafan.kotlin.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by Muyiafan on 2016/8/12.
 */
class MultiTypeAdapter(val data: MutableList<HolderContent>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val type = getItemViewType(position)
        val content = data[position]
        ViewHolderPool.getNexus(type).onBind(holder, content)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolderPool.getNexus(viewType).onCreateViewHolder(
            LayoutInflater.from(parent.context), parent
    )

    override fun getItemViewType(position: Int) = ViewHolderPool.getBeanType(data[position].javaClass)

    override fun getItemCount() = data.size
}