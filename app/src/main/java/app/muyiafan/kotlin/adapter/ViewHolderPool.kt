package app.muyiafan.kotlin.adapter

/**
 * (⁄ ⁄•⁄ω⁄•⁄ ⁄)
 * Created by ZhangWeifan on 2016/8/15.
 */
object ViewHolderPool {
    private val holderContents = mutableListOf<Class<out HolderContent>>()
    private val viewHolders = mutableListOf<HolderBeanNexus<*, *>>()
    private val nexusTags = mutableListOf<String>()
    private val viewHolderWithTags = mutableListOf<HolderBeanNexus<*, *>>()

    fun register(hc: Class<out HolderContent>, viewNexus: HolderBeanNexus<*, *>) {
        if (!holderContents.contains(hc)) {
            holderContents.add(hc)
            viewHolders.add(viewNexus)
        } else {

        }
    }

    fun register(tag: String, viewNexus: HolderBeanNexus<*, *>) {
        if (!nexusTags.contains(tag)) {
            nexusTags.add(tag)
            viewHolderWithTags.add(viewNexus)
        } else {

        }
    }

    fun getBeanType(hc: Class<out HolderContent>) = holderContents.indexOf(hc)

    fun getNexus(index: Int) = viewHolders[index]

    override fun toString() = "hcs : ${holderContents.size}, vhs : ${viewHolders.size}"

    fun getTagIndex(tag: String) = nexusTags.indexOf(tag)

    fun getNexusWithTag(index: Int) = viewHolderWithTags[index]
}