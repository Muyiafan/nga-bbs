package app.muyiafan.kotlin

import java.nio.charset.Charset
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * Created by Muyiafan on 2015/11/23.
 */

fun String.toMD5L32(): String {
    val md5 = MessageDigest.getInstance("MD5")
    val bytes = md5.digest(this.toByteArray())
    val stringBuffer = StringBuffer()
    for (b in bytes) {
        val bt = 0xff and (b.toInt())
        if (bt < 16) {
            stringBuffer.append(0)
        }
        stringBuffer.append(Integer.toHexString(bt))
    }
    return stringBuffer.toString()
}

fun String.toMD5U32(): String = this.toMD5L32().toUpperCase()

fun String.toMD5L16(): String = this.toMD5U32().substring(8, 24)

fun String.toMD5U16(): String = this.toMD5L16().toUpperCase()
private val CHINA_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.CHINA).apply { timeZone = TimeZone.getTimeZone("UTC") }
fun String.toTime(): Long = CHINA_DATE_FORMAT.parse(this).time

fun String.getDateString(nowTime: Long): String =
        if (nowTime - this.toTime() > 24 * 60 * 60 * 1000)
            SimpleDateFormat("MM-dd HH:mm", Locale.CHINA).apply { timeZone = TimeZone.getTimeZone("UTC") }.format(this.toTime())
        else
            CHINA_DATE_FORMAT.format(this.toTime()).substring(10, 16)

fun String.imgUrlHandler(wSize: Int = 500): String = "http://mc2.muyiafan.tk:3002/$this?s=fill&w=$wSize"
private val encoding = arrayOf("GB2312", "ISO-8859-1", "GBK", "UTF-8")
fun String.getEncoding() = encoding.filter { this == String(this.toByteArray(Charset.forName(it)), Charset.forName(it)) }[0]
