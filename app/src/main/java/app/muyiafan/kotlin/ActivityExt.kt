package app.muyiafan.kotlin

import android.app.Fragment
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

/**
 *
 * Created by Muyiafan on 2016/7/5.
 */
fun AppCompatActivity.showToast(text: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this.applicationContext, text, length).show()
}

@Suppress("UNCHECKED_CAST")
fun <T : Fragment> AppCompatActivity.findFragmentByTag(tag: String)
        = fragmentManager.findFragmentByTag(tag)?.let { it as? T }

@Suppress("UNCHECKED_CAST")
/*fun <T : android.support.v4.app.Fragment> AppCompatActivity.findSPFragmentByTag(tag: String)
        = supportFragmentManager.findFragmentByTag(tag)?.let { it as? T }*/
fun <T> AppCompatActivity.findSPFragmentByTag(tag: String) = supportFragmentManager.findFragmentByTag(tag)?.let { it as? T }

inline fun <reified T : Any> AppCompatActivity.intentFor(vararg params: Pair<String, Any>): Intent = Fun.createIntent(this, T::class.java, params)

inline fun <reified T : Any> AppCompatActivity.startActivity(vararg params: Pair<String, Any>) {
    startActivity(Fun.createIntent(this, T::class.java, params))
}

fun AppCompatActivity.startActivity(intent: Intent) {
    this.startActivity(intent)
}
