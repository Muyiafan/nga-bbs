package app.muyiafan.kotlin

import android.util.Log

/**
 * Log ext
 * Created by Muyiafan on 2016/2/27.
 */
inline fun <reified T> T.debug(log: Any) {
    Log.e(T::class.simpleName, log.toString())
}