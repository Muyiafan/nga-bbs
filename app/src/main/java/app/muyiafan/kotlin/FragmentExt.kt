package app.muyiafan.kotlin

import android.app.Fragment
import android.app.FragmentManager
import android.support.design.widget.Snackbar
import android.widget.Toast

/**
 * - -.
 * Created by Muyiafan on 2015/11/23.
 */

fun Fragment.startFragmentA(frameLayout: Int, manager: FragmentManager, name: String): Fragment = apply {
    manager.beginTransaction()
            .setCustomAnimations(
                    android.R.animator.fade_in,
                    android.R.animator.fade_out,
                    android.R.animator.fade_in,
                    android.R.animator.fade_out
            )
            .add(frameLayout, this, name)
            .addToBackStack(name)
            .commit()
}

fun Fragment.startFragment(layout: Int, manager: FragmentManager, name: String): Fragment = apply {
    manager.beginTransaction()
            .replace(layout, this, name)
            .addToBackStack(name)
            .commit()
}

fun Fragment.replaceFragmentA(layout: Int, manager: FragmentManager, name: String): Fragment = apply {
    manager.beginTransaction()
            .setCustomAnimations(
                    android.R.animator.fade_in,
                    android.R.animator.fade_out,
                    android.R.animator.fade_in,
                    android.R.animator.fade_out
            )
            .replace(layout, this, name)
            .commit()
}

fun Fragment.replaceFragment(layout: Int, manager: FragmentManager, name: String): Fragment = apply {
    manager.beginTransaction()
            .replace(layout, this, name)
            .commit()
}

fun Fragment.showSnackBar(text: String) {
    Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show()
}

fun Fragment.showToast(text: String) {
    Toast.makeText(activity.applicationContext, text, Toast.LENGTH_SHORT).show()
}